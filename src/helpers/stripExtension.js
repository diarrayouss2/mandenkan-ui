'use strict'

module.exports = (filename, newExtension) => {
  if (!filename) return ''
  return filename.replace(/\.[^.]+$/, '') + newExtension
}
